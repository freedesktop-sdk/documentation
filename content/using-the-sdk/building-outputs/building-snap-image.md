---
title: "Building snaps using the SDK"
linkTitle: "Building snaps"
weight: 4
infoType: task
icon: task
---
Learn how to build a snap using the Freedesktop SDK components.

<!--more-->
Freedesktop SDK provides the capability to build snaps based on Freedesktop SDK components, rather than Ubuntu Core.

{{<tip "Prerequisites">}}

- Install and configure [BuildStream](https://buildstream.build/).
    - Use BuildStream version 2.
    - For more information about using BuildStream, see the [BuildStream documentation](https://docs.buildstream.build/2.0/index.html).
- Install and configure [GNU Make](https://www.gnu.org/software/make/).
- Install [snapd](https://github.com/snapcore/snapd).
{{</tip>}}

**To build a snap, follow these steps:**

1. Open your preferred terminal and go to the root folder of the `freedesktop-sdk` repository.
2. Build and export Freedesktop SDK snaps by entering the following command:

   ```shell
   make export-snap
   ```

3. To install a snap, for example `platform.snap`, enter the following command:

   ```shell
   sudo snap install --dangerous snap/platform.snap
   ```

   {{<note "Note">}}
   The `--dangerous` option is necessary since you're installing unsigned local packages that don't come from the Snap store.
   {{</note>}}

By default, the build process creates snaps in the `snap` directory at the root of the freedesktop-sdk project.

Apart from the main snaps for the Platform and SDK runtimes, `platform.snap` and `sdk.snap`, the build process also creates the following binaries that you can use for GPU testing:

- `clinfo.snap`
- `glxinfo.snap`
- `vainfo.snap`
- `vulkaninfo.snap`
