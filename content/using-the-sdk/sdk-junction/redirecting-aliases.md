---
title: "Redirecting the SDK aliases"
weight: 3
infoType: task
---

Learn how to redirect the aliases used by Freedesktop SDK in a junction within your own project.

<!--more-->

Freedesktop SDK uses [BuildStream aliases](https://docs.buildstream.build/master/format_project.html#source-aliases) in order to abstract the download location of source code and assets. In some situations you may need to redirect these aliases so they point at different locations, an example of this is when you have mirrors of the source code and want to redirect BuildStream to use these mirrors instead of pulling from the source sites.

{{<tip "Prerequisites">}}

- Make sure you have an existing BuildStream project.
    - For more information about configuring BuildStream projects, see the [BuildStream documentation](https://docs.buildstream.build/2.0/tutorial/first-project.html).
- Create a junction element for Freedesktop SDK.
    - For more information, see [Using the SDK as a junction]({{< ref "/content/using-the-sdk/sdk-junction/_index.md" >}}).
- Make sure you have a mirror setup for a source in Freedesktop SDK.
    - For more information, see also the [BuildStream documentation](https://docs.buildstream.build/master/format_project.html#mirrors) for mirrors.
{{</tip>}}

**To redirect an alias from a Freedesktop SDK junction, follow these steps:**

Freedesktop SDK has lots of aliases, over 40 on version 24.08.14. Within Freedesktop SDK these are stored in a file called [`aliases.yml`](https://gitlab.com/freedesktop-sdk/freedesktop-sdk/-/blob/master/include/_private/aliases.yml) and are then included in the Freedesktop SDK [`project.conf`](https://gitlab.com/freedesktop-sdk/freedesktop-sdk/-/blob/master/project.conf?ref_type=heads). See also the [BuildStream documentation](https://docs.buildstream.build/master/format_project.html#mapping-source-aliases-of-subprojects) for mapping source aliases of sub-projects.

1. In your BuildStream elements directory, open your `.bst` junction element.
2. In the junction element, add the specific Freedesktop SDK aliases you wish to override, as seen in the following example:

    ```yaml
    kind: junction

        sources:
        - kind: git_repo
          url: gitlab:freedesktop-sdk/freedesktop-sdk.git

        aliases:
          pagure_releases: my_pagure_releases
    ```

    In the example, the alias `pagure_release` is being overriden by the alias `my_pagure_releases`. This allows you to add a mirror config for this alias into your `project.conf` and have elements which use the `pagure_release` alias to be pulled from this mirror instead. Lets add the mirroring config for `my_pagure_release` into the `project.conf` of your project.

3. Save the `.bst` junction element.
4. Open the `project.conf` file.
5. In this file, add some mirroring config, as seen in the following example:

    ```yaml
    name: my-super-cool-project

    min-version: 2.2

    element-path: elements
    aliases:
        my_pagure_release: https://releases.pagure.org/

    mirrors:
    - name: my-copy-of-pagure
      aliases:
        my_pagure_release:
        - http://www.my-cuper-cool-project.com/my_pagure_release/
    ```

    In the example, we have defined an alias called `my_pagure_release`, written the mirroring config for this into `project.conf` and shown how you can override an alias from Freedesktop SDK to use an alias you have defined. By adding this config it will cause all elements which use `my_pagure_release` to first attempt to pull from the defined mirror location, then falling back to the defined online location.

6. Save the `project.conf` file.
