---
title: "Junctioning from an SDK junctioned project"
weight: 4
infoType: task
---

Learn how to junction from projects which junction from Freedesktop SDK within your own project.

<!--more-->

Projects such as the [`gnome-build-meta`](https://gitlab.gnome.org/GNOME/gnome-build-meta/blob/master/elements/freedesktop-sdk.bst) define Freedesktop SDK as a junction. In order to junction from a project like this it is as easy as just creating a Junction to this project and then you should be able to use Freedesktop SDK components within your own project. However, this can increase complexity when there are more nested junctions, and you can end up in a situation where to use Freedesktop SDK elements you have to write:

```yaml
depends:
- project-reliant-on-gnome.bst:gnome-junction.bst:freedesktop-sdk.bst:bootstrap/bootstrap.bst
```

{{<tip "Prerequisites">}}

- Make sure you have an existing BuildStream project.
    - For more information about configuring BuildStream projects, see the [BuildStream documentation](https://docs.buildstream.build/2.0/tutorial/first-project.html).
- Create a junction element for gnome build meta.
    - This does not have to be a junction from gnome build meta, but this is the project that shall be used in the example below. For more information, on creating junction elements see [Using the SDK as a junction]({{< ref "/content/using-the-sdk/sdk-junction/_index.md" >}}).
{{</tip>}}

**To link to a Freedesktop SDK junction, follow these steps:**

[Link elements](https://docs.buildstream.build/master/elements/link.html) mean that you don't have to manually define a chain of junctions for an element which is inside nested sub-projects. In the example below a junction from gnome build meta is set up, then a link element is created to the freedesktop SDK junction within this project.

1. In your BuildStream elements directory, create a gnome build meta `.bst` junction element, as seen in the following example:

    ```yaml
    kind: junction

        sources:
        - kind: git_repo
          url: gnome_gitlab:GNOME/gnome-build-meta.git
    ```

    At this point elements from Freedesktop SDK can be used, but they have to be included as:

    ```yaml
    depends:
    - gnome-junction.bst:freedesktop-sdk.bst:bootstrap/bootstrap.bst
    ```

2. Save the `.bst` junction element.
3. Create a new link file, you can name this whatever you like but it is recommended to name it `freedesktop-sdk.bst`, in this file add the link config:

    ```yaml
    kind: link

    config: gnome-junction.bst:freedesktop-sdk.bst
    ```

4. Save the link file.

This also has the added benefit of if the gnome build meta project renames the junction from Freedesktop SDK then this only needs updating in this file, rather than everywhere in your project. References to elements in Freedesktop SDK now can look like this:

```yaml
depends:
# With link file
- freedesktop-sdk.bst:bootstrap/bootstrap.bst
# Without link file
- gnome-junction.bst:freedesktop-sdk.bst:bootstrap/bootstrap.bst
```
