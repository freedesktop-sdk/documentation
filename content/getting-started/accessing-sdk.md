---
title: "Accessing the SDK"
weight: 3
infoType: concept
icon: concept
---

Learn where you can find the Freedesktop SDK source code.

<!--more-->
If you want to use Freedesktop SDK for the development of your software, you can find the project on GitLab: [`gitlab.com/freedesktop-sdk/freedesktop-sdk`](https://gitlab.com/freedesktop-sdk/freedesktop-sdk)

{{<note "Note">}}
You don't need developer privileges or even a GitLab account to access and use Freedesktop SDK.
{{</note>}}
