---
title: "Pre-built outputs"
weight: 5
infoType: concept
icon: concept
---
Freedesktop SDK provides runtimes and images that you can use for testing or as a base for your own software.
<!--more-->

The pre-built Flatpak runtimes from Freedesktop SDK are available on Flathub. For more information about accessing these runtimes, see [Installing Flatpak runtimes from Flathub]({{< ref "/content/using-the-sdk/building-outputs/building-flatpak-runtime.md#installing-flatpak-runtimes-from-flathub" >}}).

The following pre-built Docker images are available in the [Freedesktop SDK Docker Hub image repository](https://hub.docker.com/u/freedesktopsdk):

- [`freedesktopsdk/sdk`](https://hub.docker.com/r/freedesktopsdk/sdk)
- [`freedesktopsdk/platform`](https://hub.docker.com/r/freedesktopsdk/platform)
- [`freedesktopsdk/flatpak`](https://hub.docker.com/r/freedesktopsdk/flatpak)
- [`freedesktopsdk/toolbox`](https://hub.docker.com/r/freedesktopsdk/toolbox)
- [`freedesktopsdk/debug`](https://hub.docker.com/r/freedesktopsdk/debug)
