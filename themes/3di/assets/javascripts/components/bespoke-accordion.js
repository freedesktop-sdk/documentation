$('[data-toggle=accordion]').on('click', function() {
    $(this).find('i').toggleClass('fa-chevron-down')
    $(this).find('i').toggleClass('fa-chevron-up')
    $(this).parent().find('.accordion--content').slideToggle()
})