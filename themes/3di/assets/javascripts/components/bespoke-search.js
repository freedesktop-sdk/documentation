var fuse;
var searchVisible = false;
var firstRun = true;
var list = $(".searchResults");
var first = list.firstChild;
var last = list.lastChild;
var maininput = $(".searchInput");
var resultsAvailable = false;

$(function () {
    loadSearch();
});

$(".searchInput").on("keyup", function () {
    executeSearch(this.value);
});

function fetchJSONFile(path, callback) {
    var httpRequest = new XMLHttpRequest();
    httpRequest.onreadystatechange = function () {
        if (httpRequest.readyState === 4) {
            if (httpRequest.status === 200) {
                var data = JSON.parse(httpRequest.responseText);
                if (callback) callback(data);
            }
        }
    };
    httpRequest.open("GET", path);
    httpRequest.send();
}

function loadSearch() {
    fetchJSONFile("/documentation/index.json", function (data) {
        var options = {
            shouldSort: true,
            ignoreLocation: true,
            findAllMatches: false,
            ignoreFieldNorm: true,
            threshold: 0.2,
            minMatchCharLength: 3,
            keys: ["title", "permalink", "contents"],
        };
        fuse = new Fuse(data, options);
    });
}

function executeSearch(term) {
    let results = fuse.search(term);
    let searchitems = "";
    let listed_urls = [];

    if (results.length === 0) {
        resultsAvailable = false;
        searchitems = "";
    } else {
        for (let item in results) {
            let permalink = results[item].item.permalink;
            if (listed_urls.includes(permalink)) {
                continue;
            }

            let description =
                results[item].item.description ||
                results[item].item.contents.slice(0, 80);
            listed_urls.push(permalink);
            searchitems =
                searchitems +
                '<li class="p-3 border-top search--result"><a class="d-flex" href="' +
                permalink +
                '" tabindex="0">' +
                '<b class="title">' +
                results[item].item.title +
                "</b><span>" +
                description +
                "</span></a></li>";
            if (listed_urls.length == 6) {
                break;
            }
        }
        resultsAvailable = true;
    }
    $(".searchResults").html(searchitems);
}
